﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Helpers;
using System.Web.Http;
using WarehouseApp.Dtos;
using WarehouseApp.Models;

namespace WarehouseApp.Controllers.api
{
    public class CustomersController : ApiController
    {
        private readonly IRepository repository;
        
        public CustomersController(IRepository repository)
        {
            this.repository = repository;   
        }

        [HttpGet]
        public IHttpActionResult GetCustomer(int id) 
        {

            var customer = repository.Get(id);

            if (customer == null)
                return Ok("{\"Bool\":false,\"Error\":\"No user with that Id.\"}");

            var customerDto = new CustomerDto
            {
                Id = customer.Id,
                Name = customer.Name,
                Quantity = customer.Quantity
            };

            return Ok(customerDto);
        }



        [HttpPost]
        public IHttpActionResult CreateCustomer([FromBody] Customer customer)
        {
            if (!ModelState.IsValid)
                return Ok("{\"Bool\":false,\"Error\":\"Name is required! Password must be between 6 and 100 characters.\"}");

            var tempCustomer = repository.GetByName(customer.Name);
            if (tempCustomer == null)
            {
                var HashPassword = Crypto.HashPassword(customer.Password);
                customer.Password = HashPassword;
                repository.Add(customer);
                var customerDto = new CustomerDto
                {
                    Bool = true,
                    Id = customer.Id,
                    Name = customer.Name,
                    Quantity = customer.Quantity
                };
                return Ok(customerDto);
            }
            return Ok("{\"Bool\":false,\"Error\":\"User allready exist.\"}");
        }


        [HttpPut]
        public IHttpActionResult UpdateCostumer(int id, [FromBody] Customer customer)
        {
            var tempCustomer = repository.Get(id);

            if (tempCustomer == null)
                return Ok("{\"Bool\":false,\"Error\":\"No user with that Id.\"}");

            tempCustomer.Quantity += customer.Quantity;
            repository.SaveChanges();
            var customerDto = new CustomerDto
            {
                Id = tempCustomer.Id,
                Name = tempCustomer.Name,
                Quantity = tempCustomer.Quantity
            };

            return Ok(customerDto);
        }


        [HttpPost]
        [Route("api/customers/login")]
        public IHttpActionResult Login([FromBody] Customer customer) //IHttpActionResult vrne 201 da je neki narejen
        {

            var tempCustomer = repository.GetByName(customer.Name);
            if (tempCustomer == null)
                return Ok("{\"Bool\":false,\"Error\":\"No user with that name.\"}");
            if (Crypto.VerifyHashedPassword(tempCustomer.Password, customer.Password))
            {
                var customerDto = new CustomerDto
                {
                    Bool = true,
                    Id = tempCustomer.Id,
                    Name = tempCustomer.Name,
                    Quantity = tempCustomer.Quantity
                };

                return Ok(customerDto);
            }
            else
            {
                return Ok("{\"Bool\":false,\"Error\":\"Wrong password.\"}");
            }
        }

    }
}
