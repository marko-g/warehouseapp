﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WarehouseApp.Dtos
{
    public class CustomerDto
    {
        public bool Bool { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
    }
}