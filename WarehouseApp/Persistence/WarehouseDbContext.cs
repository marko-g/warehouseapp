﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using WarehouseApp.Models;

namespace WarehouseApp.Persistence
{
    public class WarehouseDbContext : DbContext
    {
       public WarehouseDbContext()
       : base("DefaultConnection")
            {
            }

        public DbSet<Customer> Customers { get; set; }
    }
}