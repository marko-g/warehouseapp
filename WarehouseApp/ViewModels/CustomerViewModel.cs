﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WarehouseApp.Models;

namespace WarehouseApp.ViewModels
{
    public class CustomerViewModel
    {
        public string Title { get; set; }
        public IEnumerable<Customer> Customers { get; set; }
    }
}