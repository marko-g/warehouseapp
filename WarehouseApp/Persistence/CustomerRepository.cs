﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WarehouseApp.Models;

namespace WarehouseApp.Persistence
{
    public class CustomerRepository: IRepository
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public CustomerRepository(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public IEnumerable<Customer> GetAll()
        {
            return warehouseDbContext.Customers;
        }

        public void Add(Customer customer)
        {

            warehouseDbContext.Customers.Add(customer);
            SaveChanges();
        }

        public Customer Get(int id)
        {

            var cost = warehouseDbContext.Customers.SingleOrDefault(c => c.Id == id);


            return cost;
        }

        public Customer GetByName(string name)
        {
            return GetAll().SingleOrDefault(c => c.Name == name);
        }

        public void SaveChanges()
        {
            warehouseDbContext.SaveChanges();
        }


    }
}