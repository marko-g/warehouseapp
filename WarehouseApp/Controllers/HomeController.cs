﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WarehouseApp.Models;
using WarehouseApp.ViewModels;

namespace WarehouseApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRepository repository;
        public HomeController(IRepository repository)
        {
            this.repository = repository;
        }


        // GET: Warehouse
        public ActionResult Index()
        {

            var customerViewModel = new CustomerViewModel()
            {
                Title = "Welcome to Warehouse",
                Customers = repository.GetAll()
            };

            return View(customerViewModel);
        }



        [HttpPost]
        public ActionResult Index(string customerName)
        {
            var customerViewModel = new CustomerViewModel()
            {
                Title = "Welcome to Warehouse",
                Customers = repository.GetAll().Where(n => n.Name.ToLower().Contains(customerName.ToLower()))

            };

            return View(customerViewModel);
        }
    }
}