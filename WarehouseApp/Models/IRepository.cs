﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseApp.Models
{
    public interface IRepository
    {
        IEnumerable<Customer> GetAll();

        void Add(Customer customer);
        Customer Get(int id);

        void SaveChanges();

        Customer GetByName(string name);
    }
}
